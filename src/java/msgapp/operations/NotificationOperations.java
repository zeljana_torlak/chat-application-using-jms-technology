package msgapp.operations;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.util.logging.Level;
import java.util.logging.Logger;
import static msgapp.operations.GeneralOperations.DEFAULT_LOGO;
import static msgapp.operations.GeneralOperations.DEFAULT_PATH;

public class NotificationOperations {

    private SystemTray tray;
    private TrayIcon trayIcon;

    public NotificationOperations() {
        createTray();
    }

    private void createTray() {
        if (SystemTray.isSupported()) {
            tray = SystemTray.getSystemTray();

            java.awt.Image image = Toolkit.getDefaultToolkit().createImage(DEFAULT_PATH + DEFAULT_LOGO);

            trayIcon = new TrayIcon(image, "Chat Notification");
            trayIcon.setImageAutoSize(true);
            try {
                tray.add(trayIcon);
            } catch (AWTException ex) {
                Logger.getLogger(GeneralOperations.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            System.err.println("SystemTray is not supported on this OS");
        }

    }

    public void removeTrayIcon() {
        if (tray != null) {
            tray.remove(trayIcon);
        }
    }

    public void pushNotification(String chatName, String text) {
//        if (trayIcon == null) {
//            createTray();
//        }
        trayIcon.displayMessage(chatName, text, TrayIcon.MessageType.INFO);
    }
}
