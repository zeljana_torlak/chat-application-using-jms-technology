package msgapp.operations;

import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import msgapp.enums.MessageTypeEnum;
import msgapp.scenes.ChatsScene;

public class JMSOperations {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();

    private static ConnectionFactory connectionFactory;
    private static Topic topic;

    private JMSConsumer consumer;

    static {
        try {
            InitialContext initialContext = new InitialContext();
            connectionFactory = (ConnectionFactory) initialContext.lookup("jms/__defaultConnectionFactory");
            topic = (Topic) initialContext.lookup("msg_app_topic");
        } catch (NamingException ex) {
            Logger.getLogger(JMSOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createMessageReceiver(ChatsScene chatsScene, Korisnik receiver) {
        JMSContext context = connectionFactory.createContext();
        consumer = context.createConsumer(topic, "receiver = " + receiver.getId());

        consumer.setMessageListener((Message message) -> {
            if (message instanceof TextMessage) {
                try {
                    TextMessage textMessage = (TextMessage) message;
                    int sender = textMessage.getIntProperty("sender");
                    int chat = textMessage.getIntProperty("chat");
                    int type = textMessage.getIntProperty("type");

                    MessageTypeEnum messageType = MessageTypeEnum.values()[type];
                    switch (messageType) {
                        case NEW_CHAT:
                            Platform.runLater(() -> chatsScene.receiveNewChat(chat));
                            break;
                        case CHATNAME_CHANGED:
                            Platform.runLater(() -> chatsScene.receiveChatName(chat));
                            break;
                        case CHATCOLOR_CHANGED:
                            Platform.runLater(() -> chatsScene.receiveChatColor(chat));
                            break;
                        default:
                            String date = textMessage.getStringProperty("date");
                            String text = textMessage.getText();

                            Platform.runLater(() -> chatsScene.receiveMsg(chat, sender, text, messageType, date));
                            break;
                    }

                } catch (JMSException ex) {
                    Logger.getLogger(JMSOperations.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void stopReceiving() {
        consumer = null;
    }

    public void sendMessage(Cet chat, Korisnik sender, String text, MessageTypeEnum type, Date date) {
        try {
            JMSContext context = connectionFactory.createContext();
            JMSProducer producer = context.createProducer();
            List<Korisnik> receivers = databaseOperations.dohvatiPrimaoceCeta(chat, sender);
            for (Korisnik receiver : receivers) {
                TextMessage textMessage = context.createTextMessage();
                textMessage.setIntProperty("sender", sender.getId());
                textMessage.setIntProperty("chat", chat.getId());
                textMessage.setIntProperty("receiver", receiver.getId());
                textMessage.setIntProperty("type", type.ordinal());
                if (type != MessageTypeEnum.NEW_CHAT && type != MessageTypeEnum.CHATNAME_CHANGED && type != MessageTypeEnum.CHATCOLOR_CHANGED) {
                    textMessage.setText(text);
                    textMessage.setStringProperty("date", generalOperations.getDate(date, true));
                }
                producer.send(topic, textMessage);
            }
        } catch (JMSException ex) {
            Logger.getLogger(JMSOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
