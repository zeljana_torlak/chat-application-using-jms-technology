package msgapp.operations;

import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import msgapp.entities.Poruka;
import msgapp.entities.PripadnostCetu;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class DatabaseOperations {

    private static final EntityManager em = Persistence.createEntityManagerFactory("MsgAppPU").createEntityManager();

    private final GeneralOperations generalOperations = new GeneralOperations();

    public Korisnik napraviKorisnika(String firstName, String lastName, String username, String email, String pass, Date dateOfBirth, byte[] image) {
        Korisnik korisnik = new Korisnik();
        korisnik.setIme(firstName);
        korisnik.setPrezime(lastName);
        korisnik.setKorisnickoIme(username);
        korisnik.setMejl(email);
        try {
            korisnik.setLozinka(generalOperations.HashSHA2(pass));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DatabaseOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        korisnik.setDatumRodjenja(dateOfBirth);
        korisnik.setSlika(image);

        try {
            em.getTransaction().begin();
            em.persist(korisnik);
            em.getTransaction().commit();

            return korisnik;
        } catch (Exception e) {
            return null;
        }
    }

    public String proveriDaLiKorisnikPostoji(String username, String email) {
        List<Korisnik> resultList1 = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", username).getResultList();
        List<Korisnik> resultList2 = em.createNamedQuery("Korisnik.findByMejl", Korisnik.class).setParameter("mejl", email).getResultList();

        String s = "";
        if (resultList1 != null && !resultList1.isEmpty()) {
            s = "User with same username already exists\n";
        }
        if (resultList2 != null && !resultList2.isEmpty()) {
            s += "User with same email already exists\n";
        }

        return s;
    }

    public Korisnik dohvatiKorisnika(String usernameOrEmail, String pass) {
        List<Korisnik> resultList1 = em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", usernameOrEmail).getResultList();
        List<Korisnik> resultList2 = em.createNamedQuery("Korisnik.findByMejl", Korisnik.class).setParameter("mejl", usernameOrEmail).getResultList();

        Korisnik korisnik = null;
        if (resultList1 != null && !resultList1.isEmpty()) {
            korisnik = resultList1.get(0);
        }
        if (resultList2 != null && !resultList2.isEmpty()) {
            korisnik = resultList2.get(0);
        }

        try {
            if (korisnik == null || !korisnik.getLozinka().equals(generalOperations.HashSHA2(pass))) {
                return null;
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DatabaseOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return korisnik;
    }

    public Korisnik dohvatiKorisnika(int idKorisnik) {
        Korisnik korisnik = em.find(Korisnik.class, idKorisnik);
        em.refresh(korisnik);
        return korisnik;
    }

    public void postaviTemuKorisnika(Korisnik korisnik, int tema) {
        em.getTransaction().begin();
        korisnik.setTema(tema);
        em.getTransaction().commit();
    }

    public boolean postaviProfilnuSlikuKorisnika(Korisnik korisnik, byte[] image) {
        try {
            em.getTransaction().begin();
            korisnik.setSlika(image);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Cet dohvatiCet(int idCet) {
        Cet cet = em.find(Cet.class, idCet);
        em.refresh(cet);
        return cet;
    }

    public Cet napraviCet(String name) {
        Cet cet = new Cet();
        cet.setIme(name);
        em.getTransaction().begin();
        em.persist(cet);
        em.getTransaction().commit();
        return cet;
    }

    public void postaviImeCetu(Cet cet, String chatName) {
        em.getTransaction().begin();
        cet.setIme(chatName);
        em.getTransaction().commit();
    }

    public void postaviBojuCetu(Cet cet, int boja) {
        em.getTransaction().begin();
        cet.setBoja(boja);
        em.getTransaction().commit();
    }

    public void napraviPripadnostCetu(Korisnik korisnik, Cet cet) {
        PripadnostCetu pripadnostCetu = new PripadnostCetu();
        pripadnostCetu.setIdKorisnik(korisnik);
        pripadnostCetu.setIdCet(cet);
        em.getTransaction().begin();
        em.persist(pripadnostCetu);
        em.getTransaction().commit();
    }

    public List<Korisnik> dohvatiSveOstaleKorisnike(Korisnik korisnik) {
        return em.createQuery("SELECT k FROM Korisnik k WHERE k.id != :id", Korisnik.class).setParameter("id", korisnik.getId()).getResultList();
    }

    public List<Cet> dohvatiCetoveKorisnika(Korisnik korisnik) {
        return em.createQuery("SELECT p.idCet FROM PripadnostCetu p WHERE p.idKorisnik.id = :id", Cet.class).setParameter("id", korisnik.getId()).getResultList();
    }

    public List<Korisnik> dohvatiPrimaoceCeta(Cet cet, Korisnik posiljaoc) {
        return em.createQuery("SELECT p.idKorisnik FROM PripadnostCetu p WHERE p.idCet.id = :idCet AND p.idKorisnik.id != :idKorisnik", Korisnik.class)
                .setParameter("idCet", cet.getId()).setParameter("idKorisnik", posiljaoc.getId()).getResultList();
    }

    public Poruka dohvatiPoruku(int idPoruka) {
        Poruka poruka = em.find(Poruka.class, idPoruka);
        em.refresh(poruka);
        return poruka;
    }

    public List<Poruka> dovatiPorukeCeta(Cet cet) {
        return em.createQuery("SELECT p FROM Poruka p WHERE p.idCet.id = :idCet ORDER BY p.vreme", Poruka.class).setParameter("idCet", cet.getId()).getResultList();
    }

    public int dodajPorukuUCet(Cet cet, Korisnik posiljaoc, String sadrzaj, int tip, Date vreme) {
        Poruka poruka = new Poruka();
        poruka.setIdCet(cet);
        poruka.setIdPosiljaoc(posiljaoc);
        poruka.setSadrzaj(sadrzaj);
        poruka.setTip(tip);
        poruka.setVreme(vreme);

        try {
            em.getTransaction().begin();
            em.persist(poruka);
            em.getTransaction().commit();
            return poruka.getId();
        } catch (Exception e) {
            return -1;
        }
    }

}
