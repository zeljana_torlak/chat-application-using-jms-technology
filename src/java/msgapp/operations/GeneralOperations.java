package msgapp.operations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import msgapp.enums.GifEnum;
import msgapp.scenes.ChatsScene;
import msgapp.scenes.RegisterScene;

public class GeneralOperations {

    public enum ButtonTypeEnum {
        DEFAULT, CONFIRM_BOX, LOGIN, REGISTER_PROFILE_PIC, REGISTER_BACK, REGISTER, LOGOUT, NEW_CHATNAME
    }

    public enum IconButtonTypeEnum {
        PLUS, EDIT, PAPER_PLANE, PAPERCLIP, COG
    }

    public enum TextFieldEnum {
        LOGIN, REGISTER, TYPE_MSG, CHATNAME
    }

    public enum LabelEnum {
        DEFAULT, TITLE, REGISTER_ERROR, REGISTER_FILENAME, ERROR_MSG, USERNAME, CHATNAME, INFO_NAME, INFO
    }

    public enum FlowPaneEnum {
        DEFAULT, CONFIRM_BOX, LOGIN, REGISTER
    }

    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 800;
    public static final String BACKGROUND_COLOR_STYLE = "-fx-background-color: ";
    public static final String FONT_WEIGHT_STYLE = "-fx-font-weight: ";
    public static final String DEFAULT_PATH = "C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\";
    private static final String DEFAULT_PROFILE_PICTURE = "default-profile2.png";
    public static final String DEFAULT_LOGO = "msg1.png";

    public Button createButton(String text, ButtonTypeEnum opt, boolean defaultButton, EventHandler<ActionEvent> eventHandler) {
        Button button = new Button(text);
        button.setOnAction(eventHandler);
        switch (opt) {
            case DEFAULT:
            case CONFIRM_BOX:
                button.setPrefWidth(120);
                FlowPane.setMargin(button, new Insets(10, 10, 10, 10));
                break;
            case LOGIN:
                button.setPrefWidth(100);
                FlowPane.setMargin(button, new Insets(15, 200, 15, 200));
                break;
            case REGISTER_PROFILE_PIC:
            case REGISTER_BACK:
                button.setPrefWidth(145);
                FlowPane.setMargin(button, new Insets(10, 5, 10, 100));
                break;
            case REGISTER:
                button.setPrefWidth(145);
                FlowPane.setMargin(button, new Insets(10, 100, 10, 5));
                break;
            case LOGOUT:
                button.setPrefWidth(90);
                button.setPrefHeight(40);
                button.setMaxHeight(40);
                button.setMinHeight(40);
                FlowPane.setMargin(button, new Insets(10, 5, 10, 5));
                break;
            case NEW_CHATNAME:
                button.setPrefWidth(120);
                FlowPane.setMargin(button, new Insets(10, 35, 10, 35));
                break;
        }
        button.setDefaultButton(defaultButton);
        return button;
    }

    public Button createIconButton(IconButtonTypeEnum opt, EventHandler<ActionEvent> eventHandler) {
        Button button = new Button();
        button.setOnAction(eventHandler);
        switch (opt) {
            case PLUS:
                button.setGraphic(new ImageView(getImage("src/icons/plus.png", 30, 30)));
                FlowPane.setMargin(button, new Insets(10, 5, 10, 5));
                break;
            case EDIT:
                button.setGraphic(new ImageView(getImage("src/icons/edit.png", 30, 30)));
                FlowPane.setMargin(button, new Insets(10, 5, 10, 5));
                break;
            case PAPER_PLANE:
                button.setGraphic(new ImageView(getImage("src/icons/paper-plane.png", 30, 30)));
                FlowPane.setMargin(button, new Insets(10, 0, 10, 5));
                break;
            case PAPERCLIP:
                button.setGraphic(new ImageView(getImage("src/icons/paperclip.png", 30, 30)));
                FlowPane.setMargin(button, new Insets(10, 0, 10, 5));
                break;
            case COG:
                button.setGraphic(new ImageView(getImage("src/icons/cog.png", 30, 30)));
                FlowPane.setMargin(button, new Insets(10, 0, 10, 5));
                break;
        }
        //button.getGraphic().setOpacity(0.7);
        button.setPrefWidth(40);
        button.setMaxWidth(40);
        button.setMinWidth(40);
        button.setPrefHeight(40);
        button.setMaxHeight(40);
        button.setMinHeight(40);
        return button;
    }

    public TextField createTextField(String promptText, TextFieldEnum opt) {
        TextField textField = new TextField();
        textField.setPromptText(promptText);
        switch (opt) {
            case LOGIN:
                textField.setPrefWidth(300);
                FlowPane.setMargin(textField, new Insets(15, 100, 15, 100));
                break;
            case REGISTER:
                textField.setPrefWidth(300);
                FlowPane.setMargin(textField, new Insets(10, 10, 10, 100));
                textField.textProperty().addListener((o) -> {
                    if (textField.getText().length() > 50) {
                        textField.setText(textField.getText().substring(0, 50));
                    }
                });
                break;
            case TYPE_MSG:
                textField.setPrefWidth(370);
                textField.setMaxHeight(40);
                textField.setMinHeight(40);
                break;
            case CHATNAME:
                textField.setPrefWidth(120);
                textField.textProperty().addListener((o) -> {
                    if (textField.getText().length() > 50) {
                        textField.setText(textField.getText().substring(0, 50));
                    }
                });
                FlowPane.setMargin(textField, new Insets(10, 10, 10, 10));
                break;
        }
        return textField;
    }

    public PasswordField createPasswordField(String promptText, boolean isLogin) {
        PasswordField passField = new PasswordField();
        passField.setPromptText(promptText);
        passField.setPrefWidth(300);
        if (isLogin) {
            FlowPane.setMargin(passField, new Insets(15, 100, 15, 100));
        } else { //register 
            FlowPane.setMargin(passField, new Insets(10, 10, 10, 100));
        }
        return passField;
    }

    public Label createLabel(String text, LabelEnum opt, Color color) {
        Label label = new Label(text);
        label.setAlignment(Pos.CENTER);
        switch (opt) {
            case DEFAULT:
            case TITLE:
                label.setPrefWidth(500);
                label.setMaxWidth(500);
                break;
            case REGISTER_ERROR:
                label.setPrefWidth(140);
                label.setMaxWidth(140);
                FlowPane.setMargin(label, new Insets(10, 0, 10, 0));
                break;
            case REGISTER_FILENAME:
                label.setPrefWidth(145);
                label.setMaxWidth(145);
                FlowPane.setMargin(label, new Insets(10, 10, 10, 5));
                break;
            case ERROR_MSG:
                label.setPrefWidth(500);
                label.setMaxWidth(500);
                FlowPane.setMargin(label, new Insets(15, 0, 15, 0));
                break;
            case USERNAME:
                label.setPrefWidth(200);
                label.setMaxWidth(200);
                label.setStyle(FONT_WEIGHT_STYLE + "bold");
                label.setMinHeight(40);
                label.setMaxHeight(40);
                FlowPane.setMargin(label, new Insets(5, 0, 5, 0));
                break;
            case CHATNAME:
                label.setPrefWidth(415);
                label.setMaxWidth(415);
                label.setMinHeight(50);
                label.setMaxHeight(50);
                break;
            case INFO_NAME:
                label.setPrefWidth(90);
                label.setMaxWidth(90);
                label.setMinHeight(40);
                label.setMaxHeight(40);
                label.setStyle(FONT_WEIGHT_STYLE + "bold");
                label.setAlignment(Pos.CENTER_RIGHT);
                break;
            case INFO:
                label.setPrefWidth(190);
                label.setMaxWidth(190);
                label.setMinHeight(40);
                label.setMaxHeight(40);
                break;
        }
        if (color != null) {
            label.setTextFill(Paint.valueOf(color.toString()));
        }
        return label;
    }

    public FlowPane createFlowPane(FlowPaneEnum opt, boolean white) {
        FlowPane flowPane = new FlowPane();
        switch (opt) {
            case DEFAULT:
            case CONFIRM_BOX:
                flowPane.setPadding(new Insets(10, 10, 10, 10));
                break;
            case LOGIN:
                flowPane.setPadding(new Insets(100, 100, 100, 100));
                break;
            case REGISTER:
                flowPane.setPadding(new Insets(100, 50, 100, 100));
                break;
        }
        if (white) {
            flowPane.setStyle(BACKGROUND_COLOR_STYLE + "#ffffff;");
        }
        return flowPane;
    }

    public Separator createSeparator() {
        Separator separator = new Separator();
        separator.setPrefWidth(500);
        separator.setPrefHeight(50);
        separator.setPadding(new Insets(0, 0, 40, 0));
        return separator;
    }

    public ScrollPane createScrollPane(Node node) {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(node);
        scrollPane.setMinHeight(680);
        scrollPane.setMaxHeight(680);
        scrollPane.setPrefWidth(460);
        scrollPane.setStyle(BACKGROUND_COLOR_STYLE + "#ffffff;");
        return scrollPane;
    }

    public GridPane createGridPane() {
        GridPane gridPane = new GridPane();
        gridPane.setMinWidth(440);
        gridPane.setMaxWidth(440);
        gridPane.setMinHeight(678);
        ColumnConstraints col12 = new ColumnConstraints();
        col12.setPercentWidth(50);
        gridPane.getColumnConstraints().addAll(col12, col12);
        return gridPane;
    }

    public ListView<String> createListView(LinkedList<String> list, EventHandler<MouseEvent> eventHandler) {
        ListView<String> listView = new ListView<>();
        listView.setPrefWidth(200);
        listView.setPrefHeight(510);
        listView.getItems().addAll(list);
        listView.setOnMouseClicked(eventHandler);
        return listView;
    }

    public ComboBox<String> createComboBox(String promptText, LinkedList<String> list) {
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.getItems().addAll(list);
        comboBox.setPrefWidth(120);
        comboBox.setPromptText(promptText);
        FlowPane.setMargin(comboBox, new Insets(10, 10, 10, 10));
        return comboBox;
    }

    public DatePicker createDatePicker(String promptText) {
        DatePicker datePicker = new DatePicker();
        datePicker.setPromptText(promptText);
        datePicker.setPrefWidth(300);
        FlowPane.setMargin(datePicker, new Insets(10, 10, 10, 100));

        return datePicker;
    }

    private boolean answer;

    public boolean displayConfirmBox(Stage stage, String title, String text) {
        answer = false;
        Stage newStage = new Stage();
        newStage.setTitle(title);
        newStage.getIcons().add(getLogo());
        newStage.initModality(Modality.APPLICATION_MODAL);

        Label label = createLabel(text, LabelEnum.DEFAULT, null);
        label.setPrefWidth(270);
        label.setFont(Font.font(18));

        Button yesButton = createButton("YES", ButtonTypeEnum.CONFIRM_BOX, true, (t) -> {
            Scene scene = stage.getScene();
            if (scene instanceof ChatsScene) {
                ((ChatsScene) scene).logout();
            }
            answer = true;
            newStage.close();
        });
        Button noButton = createButton("NO", ButtonTypeEnum.CONFIRM_BOX, false, (t) -> {
            answer = false;
            newStage.close();
        });

        FlowPane layout = createFlowPane(FlowPaneEnum.CONFIRM_BOX, true);
        layout.getChildren().addAll(label, yesButton, noButton);

        Scene scene = new Scene(layout, 290, 100);
        newStage.setScene(scene);
        newStage.setResizable(false);
        newStage.setX(stage.getX() + (WINDOW_WIDTH - 290) / 2);
        newStage.setY(stage.getY() + (WINDOW_HEIGHT - 100) / 2);
        newStage.showAndWait();
        return answer;
    }

    private Image getImage(String path, double width, double height) {
        FileInputStream inputstream;
        Image image = null;
        try {
            inputstream = new FileInputStream(path);
            image = new Image(inputstream, width, height, true, true);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GeneralOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }

    public Image getLogo() {
        return getImage(DEFAULT_PATH + DEFAULT_LOGO, 300, 300);
    }

    public ImageView createLogo() {
        ImageView imageView = new ImageView(getLogo());
        imageView.setFitHeight(180);
        imageView.setFitWidth(180);
        imageView.setPreserveRatio(true);
        FlowPane.setMargin(imageView, new Insets(0, 160, 0, 160));
        return imageView;
    }

    public Image byteArrayToImage(byte[] img) {
        return new Image(new ByteArrayInputStream(img), 2000, 2000, true, true);
    }

    public ImageView createProfilePicture(byte[] img) {
        ImageView imageView = new ImageView(byteArrayToImage(img));
        imageView.setFitHeight(150);
        imageView.setFitWidth(150);
        imageView.setPreserveRatio(true);
        Circle clip = new Circle(75, 75, 75);
        imageView.setClip(clip);
        FlowPane.setMargin(imageView, new Insets(10, 25, 10, 25));
        return imageView;
    }

    public void createPauseTransition(int duration_in_seconds, EventHandler<ActionEvent> eventHandler) {
        PauseTransition delay = new PauseTransition(Duration.seconds(duration_in_seconds));
        delay.setOnFinished(eventHandler);
        delay.play();
    }

    public void createGifInRegister(RegisterScene registerScene) {
        registerScene.setImageOfGif(getImage(DEFAULT_PATH + GifEnum.CONFETTI.getName(), GifEnum.CONFETTI.getWidth(), GifEnum.CONFETTI.getHeight()));
        createPauseTransition(5, (t) -> registerScene.setImageOfGif(null));
    }

    public void createGifInChats(ChatsScene chatsScene, GifEnum gifType) {//
        int width = gifType.getWidth();
        int height = gifType.getHeight();
        if (width > 440)
            width = 440;
        if (height > 678)
            height = 678;
        chatsScene.setImageOfGif(getImage(DEFAULT_PATH + gifType.getName(), width, height));
        createPauseTransition(3, (t) -> chatsScene.setImageOfGif(null));
    }

    public Image getImageOfConffetiGif() {
        return getImage(DEFAULT_PATH + GifEnum.CONFETTI.getName(), GifEnum.CONFETTI.getWidth() / 2, GifEnum.CONFETTI.getHeight() / 2);
    }

    public byte[] convertImageFileToArrayOfBytes(File file) {
        byte[] image = null;
        try {
            if (file != null) {
                image = Files.readAllBytes(file.toPath());
            } else {
                image = Files.readAllBytes(Paths.get(DEFAULT_PATH + DEFAULT_PROFILE_PICTURE));
            }
        } catch (IOException ex) {
            Logger.getLogger(GeneralOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return image;
    }

    public String convertFileToString(File file) {
        String s = null;
        if (file != null) {
            try {
                byte[] bs = Files.readAllBytes(file.toPath());
                s = Base64.getEncoder().encodeToString(bs);
            } catch (IOException ex) {
                Logger.getLogger(GeneralOperations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return s;
    }

    public void convertStringToFile(String fileName, String content, File directoryFile) {
        try {
            if (directoryFile != null) {
                File file = new File(directoryFile.getPath() + "\\" + fileName);
                Files.write(file.toPath(), Base64.getDecoder().decode(content));
            }
        } catch (IOException ex) {
            Logger.getLogger(GeneralOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String HashSHA2(String value) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedhash = digest.digest(value.getBytes(StandardCharsets.UTF_8));

        StringBuilder builder = new StringBuilder();
        for (byte b : encodedhash) {
            builder.append(Integer.toString((b & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return builder.toString();
    }

    public String getDate(Date datum, boolean isFull) {
        SimpleDateFormat sdf;
        if (isFull) {
            sdf = new SimpleDateFormat("dd.MM.yyyy.HH:mm:ss");
        } else {
            sdf = new SimpleDateFormat("dd.MM.yyyy.");
        }
        return sdf.format(datum);
    }

    public Tooltip createToolTip(String s) {
        Tooltip tooltip = new Tooltip(s);
        return tooltip;
    }

    public FileChooser createProfilePictureChooser() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("images", "*.jpg", "*.jpeg", "*.png", "*.gif", "*.bmp");
        fileChooser.getExtensionFilters().add(filter);
        //fileChooser.setSelectedExtensionFilter(filter);

        return fileChooser;
    }

}
