package msgapp.enums;

public enum ChatColorEnum {
    
    BLUE("#76bddb"),
    ROSE("#ffc2d5"),
    PINKY("#ff9eff"),
    VIOLET("#e0abff"),
    RED("#fa9691"),
    ORANGE("#facea5"),
    YELLOW("#fff98c"),
    LIME("#a8f25e"),
    GREEN("#94ffc4"),
    DARK_GREEN("#34c993"),
    LIGHT_BLUE("#bafff1"),
    GRAY("#ced9d7");
    
    private final String rgb;

    ChatColorEnum(String s) {
        this.rgb = s;
    }
    
    public String getColorCode(){
        return rgb;
    }
    
}
