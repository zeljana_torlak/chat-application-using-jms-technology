package msgapp.enums;

public enum MessageTypeEnum {

    TEXT, 
    FILE, 
    ERROR, 
    NEW_CHAT,
    CHATNAME_CHANGED, 
    CHATCOLOR_CHANGED

}
