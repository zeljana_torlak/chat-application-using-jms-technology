package msgapp.enums;

public enum GifEnum {
    CONFETTI("confetti.gif", 400, 224), //390, 242
    STARS("stars.gif", 332, 600),
    STAR("star.gif", 1080, 1080),
    LEAF("leaf.gif", 1000, 1000),
    BALLOON("balloon.gif", 640, 640);
    
    private final String name;
    private final int width, height;

    GifEnum(String gifName, int gifWidth, int gifheight) {
        this.name = gifName;
        this.width = gifWidth;
        this.height = gifheight;
    }
    
    public String getName(){
        return name;
    }
    
    public int getWidth(){
        return width;
    }
    
    public int getHeight(){
        return height;
    }
    
}
