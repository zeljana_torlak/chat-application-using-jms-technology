package msgapp.stages;

import msgapp.entities.Korisnik;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import msgapp.operations.GeneralOperations;
import static msgapp.operations.GeneralOperations.WINDOW_HEIGHT;
import static msgapp.operations.GeneralOperations.WINDOW_WIDTH;

public class UserInfoStage extends Stage {

    private final GeneralOperations generalOperations = new GeneralOperations();
    
    private final Stage parentStage;

    public UserInfoStage(Stage st, Korisnik korisnik) {
        parentStage = st;
        
        ImageView profilePicture = generalOperations.createProfilePicture(korisnik.getSlika());
        FlowPane.setMargin(profilePicture, new Insets(15, 65, 15, 65));

        Label firstNameLabel = generalOperations.createLabel("First name: ", GeneralOperations.LabelEnum.INFO_NAME, null);
        Label firstName = generalOperations.createLabel(korisnik.getIme(), GeneralOperations.LabelEnum.INFO, null);
        
        Label lastNameLabel = generalOperations.createLabel("Last name: ", GeneralOperations.LabelEnum.INFO_NAME, null);
        Label lastName = generalOperations.createLabel(korisnik.getPrezime(), GeneralOperations.LabelEnum.INFO, null);
        
        Label usernameLabel = generalOperations.createLabel("Username: ", GeneralOperations.LabelEnum.INFO_NAME, null);
        Label username = generalOperations.createLabel(korisnik.getKorisnickoIme(), GeneralOperations.LabelEnum.INFO, null);
        
        Label emailLabel = generalOperations.createLabel("Email: ", GeneralOperations.LabelEnum.INFO_NAME, null);
        Label email = generalOperations.createLabel(korisnik.getMejl(), GeneralOperations.LabelEnum.INFO, null);
        
        Label dateOfBirthLabel = generalOperations.createLabel("Birthday: ", GeneralOperations.LabelEnum.INFO_NAME, null);
        Label dateOfBirth = generalOperations.createLabel(generalOperations.getDate(korisnik.getDatumRodjenja(), false), GeneralOperations.LabelEnum.INFO, null);

        FlowPane layout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, true);
        layout.getChildren().addAll(profilePicture, firstNameLabel, firstName, lastNameLabel, lastName, 
                usernameLabel, username, emailLabel, email, dateOfBirthLabel, dateOfBirth);

        Scene scene = new Scene(layout, 300, 400);
        setScene(scene);
        setTitle("User info");
        getIcons().add(generalOperations.getLogo());
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);
        setX(parentStage.getX() + (WINDOW_WIDTH - 300) / 2);
        setY(parentStage.getY() + (WINDOW_HEIGHT - 400) / 2);
        showAndWait();

    }
    
}
