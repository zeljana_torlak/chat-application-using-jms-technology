package msgapp.stages;

import msgapp.entities.Korisnik;
import java.io.File;
import java.util.LinkedList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import msgapp.enums.ChatThemeEnum;
import msgapp.operations.DatabaseOperations;
import msgapp.operations.GeneralOperations;
import static msgapp.operations.GeneralOperations.WINDOW_HEIGHT;
import static msgapp.operations.GeneralOperations.WINDOW_WIDTH;
import msgapp.scenes.ChatsScene;

public class EditUserStage extends Stage {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();

    private final FileChooser fileChooser;
    private final Label fileMsg;

    private final ComboBox<String> comboBox;
    
    private final Stage parentStage;
    private final ChatsScene chatsScene;

    public EditUserStage(Stage st, ChatsScene chats, Korisnik korisnik) {
        parentStage = st;
        chatsScene = chats;
        
        LinkedList<String> lista = new LinkedList<>();
        for (ChatThemeEnum t : ChatThemeEnum.values()) {
            lista.add(t.name());
        }
        comboBox = generalOperations.createComboBox("theme", lista);
        comboBox.valueProperty().addListener((t) -> changeTheme(korisnik));

        fileChooser = generalOperations.createProfilePictureChooser();
        fileMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.ERROR_MSG, Color.RED);
        Button openButton = generalOperations.createButton("profile picture", GeneralOperations.ButtonTypeEnum.DEFAULT, false, (t) -> changeProfilePicture(korisnik));
        openButton.setTooltip(generalOperations.createToolTip("Maximum size 16777215 bytes (16 MB)"));

        FlowPane layout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, true);
        layout.getChildren().addAll(comboBox, openButton, fileMsg);

        Scene scene = new Scene(layout, 290, 100);
        setScene(scene);
        setTitle("Edit user");
        getIcons().add(generalOperations.getLogo());
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);
        setX(parentStage.getX() + (WINDOW_WIDTH - 290) / 2);
        setY(parentStage.getY() + (WINDOW_HEIGHT - 100) / 2);
        showAndWait();

    }

    private void changeProfilePicture(Korisnik korisnik) {
        File file = fileChooser.showOpenDialog(parentStage);

        byte[] image = generalOperations.convertImageFileToArrayOfBytes(file);
        fileMsg.setText("");

        if (!databaseOperations.postaviProfilnuSlikuKorisnika(korisnik, image))
            fileMsg.setText("Error occurred.");
        chatsScene.updateProfilePicture(generalOperations.byteArrayToImage(image));
    }

    private void changeTheme(Korisnik korisnik) {
        int index = comboBox.getSelectionModel().getSelectedIndex();
        if (index < 0) {
            return;
        }
        databaseOperations.postaviTemuKorisnika(korisnik, ChatThemeEnum.values()[index].ordinal());
        chatsScene.updateTheme(ChatThemeEnum.values()[index]);
        comboBox.getSelectionModel().clearSelection();
    }

}
