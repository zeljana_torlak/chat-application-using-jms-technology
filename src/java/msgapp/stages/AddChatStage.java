package msgapp.stages;

import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import msgapp.enums.MessageTypeEnum;
import msgapp.operations.DatabaseOperations;
import msgapp.operations.GeneralOperations;
import static msgapp.operations.GeneralOperations.WINDOW_HEIGHT;
import static msgapp.operations.GeneralOperations.WINDOW_WIDTH;
import msgapp.operations.JMSOperations;
import msgapp.scenes.ChatsScene;

public class AddChatStage extends Stage {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();
    private final JMSOperations jmsOperations = new JMSOperations();

    private final TextField chatNameInput;
    private final ListView<String> listView;
    private final List<Korisnik> sviKorisnici;

    private final ImageView gifView;

    private final Stage parentStage;
    private final ChatsScene chatsScene;

    public AddChatStage(Stage st, ChatsScene chats, Korisnik korisnik) {
        parentStage = st;
        chatsScene = chats;

        chatNameInput = generalOperations.createTextField("chat name", GeneralOperations.TextFieldEnum.CHATNAME);
        chatNameInput.setPrefWidth(170);

        sviKorisnici = databaseOperations.dohvatiSveOstaleKorisnike(korisnik);
        LinkedList<String> imenaSvihKorisnika = new LinkedList<>();
        sviKorisnici.forEach((k) -> imenaSvihKorisnika.add(k.getKorisnickoIme()));

        listView = new ListView<>();//"member"
        ObservableList<String> list = FXCollections.observableArrayList();
        listView.setItems(list);
        list.addAll(imenaSvihKorisnika);
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setPrefWidth(170);
        listView.setPrefHeight(110);
        FlowPane.setMargin(listView, new Insets(10, 10, 10, 10));

        gifView = new ImageView();

        Button createButton = generalOperations.createButton("CREATE", GeneralOperations.ButtonTypeEnum.NEW_CHATNAME, true, (t) -> addChat(korisnik));

        FlowPane layout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, true);
        layout.getChildren().addAll(chatNameInput, listView, createButton);
        StackPane stackPane = new StackPane(layout, gifView);

        Scene scene = new Scene(stackPane, 200, 250);
        setScene(scene);
        setTitle("New chat");
        getIcons().add(generalOperations.getLogo());
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);
        setX(parentStage.getX() + (WINDOW_WIDTH - 200) / 2);
        setY(parentStage.getY() + (WINDOW_HEIGHT - 250) / 2);
        showAndWait();
    }

    private void addChat(Korisnik korisnik) {
        ObservableList<Integer> selectedIndexes = listView.getSelectionModel().getSelectedIndices();
        String chatName = chatNameInput.getText();
        if (chatName.isEmpty() || selectedIndexes.isEmpty()) {
            return;
        }

        Cet cet = databaseOperations.napraviCet(chatName);
        databaseOperations.napraviPripadnostCetu(korisnik, cet); //host

        for (Integer i : selectedIndexes) {
            Korisnik korisnik2 = sviKorisnici.get(i);
            databaseOperations.napraviPripadnostCetu(korisnik2, cet);
        }

        chatsScene.updateChatListView(cet);
        jmsOperations.sendMessage(cet, korisnik, null, MessageTypeEnum.NEW_CHAT, null);
        
        gifView.setImage(generalOperations.getImageOfConffetiGif());
        generalOperations.createPauseTransition(3, (tt) -> {
            gifView.setImage(null);
            this.close();
        });
        
        chatsScene.selectNewChat();
    }

}
