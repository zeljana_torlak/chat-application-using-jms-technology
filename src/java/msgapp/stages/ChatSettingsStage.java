package msgapp.stages;

import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import java.util.LinkedList;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import msgapp.enums.ChatColorEnum;
import msgapp.enums.MessageTypeEnum;
import msgapp.operations.DatabaseOperations;
import msgapp.operations.GeneralOperations;
import static msgapp.operations.GeneralOperations.WINDOW_HEIGHT;
import static msgapp.operations.GeneralOperations.WINDOW_WIDTH;
import msgapp.operations.JMSOperations;
import msgapp.scenes.ChatsScene;

public class ChatSettingsStage extends Stage {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();
    private final JMSOperations jmsOperations = new JMSOperations();
    
    private final ComboBox<String> comboBox;
    private final TextField chatNameInput;
    
    private final Stage parentStage;
    private final ChatsScene chatsScene;
    
    public ChatSettingsStage(Stage st, ChatsScene chats, Korisnik korisnik, Cet cet){
        parentStage = st;
        chatsScene = chats;
        
        chatNameInput = generalOperations.createTextField("chat name", GeneralOperations.TextFieldEnum.CHATNAME);
        chatNameInput.setOnKeyPressed((t) -> {
            if (t.getCode().equals(KeyCode.ENTER)) {
                changeChatName(korisnik, cet);
            }
        });
        //Button changeButton = generalOperations.createButton("change", GeneralOperations.ButtonTypeEnum.DEFAULT, false, (t) -> changeChatName(korisnik, cet));

        LinkedList<String> lista = new LinkedList<>();
        for (ChatColorEnum t : ChatColorEnum.values()) {
            lista.add(t.name());
        }
        comboBox = generalOperations.createComboBox("color", lista);
        comboBox.valueProperty().addListener((t) -> changeColor(korisnik, cet));

        FlowPane layout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, true);
        layout.getChildren().addAll(chatNameInput, comboBox);

        Scene scene = new Scene(layout, 290, 100);
        setScene(scene);
        setTitle("Chat settings");
        getIcons().add(generalOperations.getLogo());
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);
        setX(parentStage.getX() + (WINDOW_WIDTH - 290) / 2);
        setY(parentStage.getY() + (WINDOW_HEIGHT - 100) / 2);
        showAndWait();

    }

    private void changeChatName(Korisnik korisnik, Cet cet) {
        String chatName = chatNameInput.getText();
        if (chatName.isEmpty()) {
            return;
        }
        
        databaseOperations.postaviImeCetu(cet, chatName);
        chatsScene.changeCetInChatList(cet.getId());
        chatsScene.updateChatName(chatName);
        jmsOperations.sendMessage(cet, korisnik, null, MessageTypeEnum.CHATNAME_CHANGED, null);
        chatNameInput.clear();
    }

    private void changeColor(Korisnik korisnik, Cet cet) {
        int index = comboBox.getSelectionModel().getSelectedIndex();
        if (index < 0) {
            return;
        }
        databaseOperations.postaviBojuCetu(cet, ChatColorEnum.values()[index].ordinal());
        chatsScene.changeCetInChatList(cet.getId());
        chatsScene.updateChatColor(ChatColorEnum.values()[index]);
        jmsOperations.sendMessage(cet, korisnik, null, MessageTypeEnum.CHATCOLOR_CHANGED, null);
        comboBox.getSelectionModel().clearSelection();
    }
}
