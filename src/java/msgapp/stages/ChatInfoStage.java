package msgapp.stages;

import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import java.util.List;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import msgapp.operations.DatabaseOperations;
import msgapp.operations.GeneralOperations;
import static msgapp.operations.GeneralOperations.WINDOW_HEIGHT;
import static msgapp.operations.GeneralOperations.WINDOW_WIDTH;

public class ChatInfoStage extends Stage {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();

    private final Stage parentStage;

    public ChatInfoStage(Stage st, Korisnik korisnik, Cet cet) {
        parentStage = st;

        Label membersLabel = generalOperations.createLabel("Members: ", GeneralOperations.LabelEnum.INFO_NAME, null);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefWidth(210);
        scrollPane.setPrefHeight(160);
        FlowPane flowPane = new FlowPane();
        flowPane.setMaxWidth(190);
        scrollPane.setContent(flowPane);

        List<Korisnik> members = databaseOperations.dohvatiPrimaoceCeta(cet, korisnik);
        members.add(korisnik);

        for (Korisnik k : members) {
            Label memberUsername = generalOperations.createLabel(k.getKorisnickoIme(), GeneralOperations.LabelEnum.INFO, null);
            memberUsername.setOnMouseClicked((t) -> new UserInfoStage(parentStage, k));
            flowPane.getChildren().add(memberUsername);

        }

        FlowPane layout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, true);
        layout.getChildren().addAll(membersLabel, scrollPane);
        
        Scene scene = new Scene(layout, 220, 210);
        setScene(scene);
        setTitle("Chat info");
        getIcons().add(generalOperations.getLogo());
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);
        setX(parentStage.getX() + (WINDOW_WIDTH - 220) / 2);
        setY(parentStage.getY() + (WINDOW_HEIGHT - 210) / 2);
        showAndWait();

    }

}
