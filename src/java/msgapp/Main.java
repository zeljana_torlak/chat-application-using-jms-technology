package msgapp;

import msgapp.scenes.LoginScene;
import msgapp.operations.GeneralOperations;
import javafx.application.Application;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import msgapp.operations.JMSOperations;

public class Main extends Application {

    private final GeneralOperations generalOperations = new GeneralOperations();

    public static void main(String[] args) {
        new Thread(JMSOperations::new).start(); //radi ubrzavanja aplikacije
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setOnCloseRequest((t) -> {
            t.consume();
            if (generalOperations.displayConfirmBox(stage, "Exit", "Are you sure you want to exit?")) {
                stage.close();
            }
        });

        stage.setScene(new LoginScene(new FlowPane(), stage));
        stage.setTitle("Messenger");
        stage.getIcons().add(generalOperations.getLogo());
        stage.setResizable(false);
        stage.show();
    }

}
