package msgapp.scenes;

import msgapp.operations.GeneralOperations;
import msgapp.operations.DatabaseOperations;
import msgapp.entities.Korisnik;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class LoginScene extends Scene {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();

    private final TextField nameInput;
    private final PasswordField passInput;

    private final Label msg;
    
    private final Stage stage;

    public LoginScene(Parent parent, Stage st) {
        super(parent, GeneralOperations.WINDOW_WIDTH, GeneralOperations.WINDOW_HEIGHT);
        stage = st;

        ImageView imageView = generalOperations.createLogo();
        Label welcomeLabel = generalOperations.createLabel("Welcome to Chat App", GeneralOperations.LabelEnum.TITLE, null);
        welcomeLabel.setFont(Font.font(36));
        Separator separator = generalOperations.createSeparator();

        nameInput = generalOperations.createTextField("username or email", GeneralOperations.TextFieldEnum.LOGIN);
        passInput = generalOperations.createPasswordField("password", true);

        Button loginButton = generalOperations.createButton("LOGIN", GeneralOperations.ButtonTypeEnum.LOGIN, true, (t) -> doLogin());

        msg = generalOperations.createLabel("", GeneralOperations.LabelEnum.ERROR_MSG, Color.RED);
        msg.setMinHeight(20);
        msg.setMaxHeight(20);

        Button registerButton = generalOperations.createButton("REGISTER", GeneralOperations.ButtonTypeEnum.LOGIN, false, (t)
                -> stage.setScene(new RegisterScene(new FlowPane(), stage)));

        FlowPane loginLayout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.LOGIN, true);
        loginLayout.getChildren().addAll(imageView, welcomeLabel, separator, nameInput, passInput, loginButton, msg, registerButton);
        this.setRoot(loginLayout);
    }

    private void doLogin() {
        String name = nameInput.getText();
        String pass = passInput.getText();
        msg.setText("");
        passInput.clear();

        Korisnik korisnik = databaseOperations.dohvatiKorisnika(name, pass);
        if (korisnik == null) {
            msg.setText("User or password is not correct");
            return;
        }

        ChatsScene chatsScene = new ChatsScene(new FlowPane(), stage, korisnik);
        stage.setScene(chatsScene);
    }

}
