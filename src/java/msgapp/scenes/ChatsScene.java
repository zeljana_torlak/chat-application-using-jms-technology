package msgapp.scenes;

import msgapp.operations.GeneralOperations;
import msgapp.operations.DatabaseOperations;
import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import msgapp.entities.Poruka;
import java.io.File;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import msgapp.enums.ChatColorEnum;
import msgapp.enums.MessageTypeEnum;
import msgapp.enums.ChatThemeEnum;
import msgapp.enums.GifEnum;
import static msgapp.operations.GeneralOperations.BACKGROUND_COLOR_STYLE;
import msgapp.operations.JMSOperations;
import msgapp.operations.NotificationOperations;
import msgapp.stages.AddChatStage;
import msgapp.stages.ChatInfoStage;
import msgapp.stages.ChatSettingsStage;
import msgapp.stages.EditUserStage;
import msgapp.stages.UserInfoStage;

public class ChatsScene extends Scene {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();
    private final JMSOperations jmsOperations = new JMSOperations();
    private final NotificationOperations notificationOperations = new NotificationOperations();

    private Korisnik korisnik = null;
    private Cet selectedCet = null;
    private int selectedIndex = -1;

    private final List<Cet> cetoviKorisnika;
    private final LinkedList<String> imenaCetovaKorisnika = new LinkedList<>();

    private final ImageView profilePicture;
    private final ListView<String> chatList;
    private final Label chatNameLabel;
    private final TextField msgToSendInput;
    private final Button settingsButton, sendButton, browseButton;

    private final List<TextArea> sentMessages = new LinkedList<>();
    private final GridPane chatBox;
    private final ScrollPane container;

    private final FlowPane chatsLeftLayout, chatsCenterLayout;
    private final ImageView gifView;

    private int rowIndex = 0;

    private final Stage stage;

    public ChatsScene(Parent parent, Stage st, Korisnik k) {
        super(parent, GeneralOperations.WINDOW_WIDTH, GeneralOperations.WINDOW_HEIGHT);
        stage = st;

        korisnik = k;
        jmsOperations.createMessageReceiver(this, korisnik); //

        //left part -------------------------------------
        profilePicture = generalOperations.createProfilePicture(korisnik.getSlika());
        profilePicture.setOnMouseClicked((t) -> new UserInfoStage(stage, korisnik));
        Label usernameLabel = generalOperations.createLabel("" + korisnik.getKorisnickoIme(), GeneralOperations.LabelEnum.USERNAME, null);
        usernameLabel.setFont(Font.font(20));
        Button logoutButton = generalOperations.createButton("LOGOUT", GeneralOperations.ButtonTypeEnum.LOGOUT, false, (t) -> {
            logout();
            stage.setScene(new LoginScene(new FlowPane(), stage));
        });

        Button plusButton = generalOperations.createIconButton(GeneralOperations.IconButtonTypeEnum.PLUS, (t) -> new AddChatStage(stage, this, korisnik));
        Button editButton = generalOperations.createIconButton(GeneralOperations.IconButtonTypeEnum.EDIT, (t) -> new EditUserStage(stage, this, korisnik));

        cetoviKorisnika = databaseOperations.dohvatiCetoveKorisnika(korisnik);
        cetoviKorisnika.forEach((c) -> imenaCetovaKorisnika.add(c.getIme()));
        chatList = generalOperations.createListView(imenaCetovaKorisnika, (t) -> selectCet());

        //center part -------------------------------------
        chatNameLabel = generalOperations.createLabel("", GeneralOperations.LabelEnum.CHATNAME, null);
        chatNameLabel.setFont(Font.font(24));
        chatNameLabel.setOnMouseClicked((t) -> new ChatInfoStage(stage, korisnik, selectedCet));
        settingsButton = generalOperations.createIconButton(GeneralOperations.IconButtonTypeEnum.COG, (t) -> new ChatSettingsStage(stage, this, korisnik, selectedCet));
        settingsButton.setVisible(false);

        chatBox = generalOperations.createGridPane();
        container = generalOperations.createScrollPane(chatBox);
        container.setVisible(false);
        container.vvalueProperty().bind(chatBox.heightProperty());

        msgToSendInput = generalOperations.createTextField("type message", GeneralOperations.TextFieldEnum.TYPE_MSG);
        msgToSendInput.setTooltip(generalOperations.createToolTip("Maximum message size 16777215 bytes (16 MB)"));
        msgToSendInput.setVisible(false);
        msgToSendInput.setOnKeyPressed((t) -> {
            if (t.getCode().equals(KeyCode.ENTER)) {
                sendMsg(msgToSendInput.getText(), MessageTypeEnum.TEXT);
            }
        });
        sendButton = generalOperations.createIconButton(GeneralOperations.IconButtonTypeEnum.PAPER_PLANE, (t) -> sendMsg(msgToSendInput.getText(), MessageTypeEnum.TEXT));
        sendButton.setVisible(false);
        FileChooser fileChooser = new FileChooser();
        browseButton = generalOperations.createIconButton(GeneralOperations.IconButtonTypeEnum.PAPERCLIP, (t) -> {
            File file = fileChooser.showOpenDialog(stage);

            if (file != null) {
                String fileString = generalOperations.convertFileToString(file);
                String text = file.getName() + ":" + fileString;
                sendMsg(text, MessageTypeEnum.FILE);
            }
        });
        browseButton.setVisible(false);

        chatsLeftLayout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, false);
        chatsLeftLayout.getChildren().addAll(profilePicture, usernameLabel, logoutButton, plusButton, editButton, chatList);
        chatsLeftLayout.setPrefWidth(220);
        chatsCenterLayout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.DEFAULT, false);
        chatsCenterLayout.getChildren().addAll(chatNameLabel, settingsButton, container, msgToSendInput, sendButton, browseButton);

        BorderPane chatsLayout = new BorderPane();
        chatsLayout.setLeft(chatsLeftLayout);
        chatsLayout.setCenter(chatsCenterLayout);

        gifView = new ImageView();
        StackPane.setMargin(gifView, new Insets(60, 10, 60, 230));
        StackPane stackPane = new StackPane(chatsLayout, gifView);

        updateTheme(ChatThemeEnum.values()[korisnik.getTema()]);
        this.setRoot(stackPane);
    }

    private void selectCet() {
        int selectedIndexTmp = chatList.getSelectionModel().getSelectedIndex();
        if (selectedIndexTmp < 0) {
            return;
        }
        if (selectedIndexTmp == selectedIndex) {
            chatList.getSelectionModel().clearSelection();
            selectedIndex = -1;
            selectedCet = null;
            chatNameLabel.setText("");
            settingsButton.setVisible(false);
            msgToSendInput.setVisible(false);
            sendButton.setVisible(false);
            browseButton.setVisible(false);
            container.setVisible(false);
            return;
        }
        selectedIndex = selectedIndexTmp;
        selectedCet = cetoviKorisnika.get(selectedIndex);
        chatNameLabel.setText(selectedCet.getIme());
        settingsButton.setVisible(true);
        msgToSendInput.setVisible(true);
        sendButton.setVisible(true);
        browseButton.setVisible(true);
        container.setVisible(true);

        sentMessages.clear();
        chatBox.getChildren().clear();
        rowIndex = 0;
        msgToSendInput.setText("");

        List<Poruka> poruke = databaseOperations.dovatiPorukeCeta(selectedCet);
        if (poruke == null) {
            return;
        }

        poruke.forEach((p)
                -> addMsg(p.getIdPosiljaoc().getId(), p.getSadrzaj(), MessageTypeEnum.values()[p.getTip()], generalOperations.getDate(p.getVreme(), true))
        );

    }

    public void receiveMsg(int idCet, int sender, String text, MessageTypeEnum type, String date) {
        String sadrzaj = null;
        if (type == MessageTypeEnum.FILE) {
            sadrzaj = databaseOperations.dohvatiPoruku(Integer.parseInt(text)).getSadrzaj();
        }
        if (selectedCet != null && selectedCet.getId() == idCet) {
            if (type == MessageTypeEnum.FILE) {
                addMsg(sender, sadrzaj, type, date);
            } else {
                addMsg(sender, text, type, date);
            }

            checkSpecialWord(type, text);;
        } else {
            Cet chat = databaseOperations.dohvatiCet(idCet);
            if (type == MessageTypeEnum.FILE) {
                notificationOperations.pushNotification(chat.getIme(), sadrzaj.substring(0, sadrzaj.indexOf(':')));
            } else {
                notificationOperations.pushNotification(chat.getIme(), text);
            }
        }
    }

    public Cet changeCetInChatList(int idCet) {
        Cet changedChat = databaseOperations.dohvatiCet(idCet);
        for (int i = 0; i < cetoviKorisnika.size(); i++) {
            if (cetoviKorisnika.get(i).getId() == idCet) {
                cetoviKorisnika.set(i, changedChat);
                imenaCetovaKorisnika.set(i, changedChat.getIme());
                break;
            }
        }
        chatList.getItems().clear();
        chatList.getItems().addAll(imenaCetovaKorisnika);
        return changedChat;
    }

    public void receiveChatName(int idCet) {
        Cet changedChat = changeCetInChatList(idCet);
        if (selectedCet != null && selectedCet.getId() == idCet) {
            updateChatName(changedChat.getIme());
        } else {
            notificationOperations.pushNotification(changedChat.getIme(), "Chat name changed.");
        }
    }

    public void receiveChatColor(int idCet) {
        Cet changedChat = changeCetInChatList(idCet);
        if (selectedCet != null && selectedCet.getId() == idCet) {
            updateChatColor(ChatColorEnum.values()[changedChat.getBoja()]);
        } else {
            notificationOperations.pushNotification(changedChat.getIme(), "Chat color changed.");
        }
    }

    public void receiveNewChat(int idCet) {
        Cet cet = databaseOperations.dohvatiCet(idCet);
        updateChatListView(cet);
        notificationOperations.pushNotification(cet.getIme(), "New chat created.");
    }

    private void sendMsg(String text, MessageTypeEnum tip) {
        Date vreme = new Date();
        msgToSendInput.clear();

        int idPoruke = databaseOperations.dodajPorukuUCet(selectedCet, korisnik, text, tip.ordinal(), vreme);
        if (idPoruke == -1) {
            addMsg(korisnik.getId(), "Message could not be sent.", MessageTypeEnum.ERROR, generalOperations.getDate(vreme, true));
            return;
        }
        Platform.runLater(() -> {
            if (tip == MessageTypeEnum.FILE) {
                jmsOperations.sendMessage(selectedCet, korisnik, "" + idPoruke, tip, vreme);
            } else {
                jmsOperations.sendMessage(selectedCet, korisnik, text, tip, vreme);
            }
        });
        addMsg(korisnik.getId(), text, tip, generalOperations.getDate(vreme, true));

        checkSpecialWord(tip, text);
    }

    private void checkSpecialWord(MessageTypeEnum type, String text) {
        if (type == MessageTypeEnum.TEXT) {
            for (GifEnum g : GifEnum.values()) {
                if (text.equalsIgnoreCase(g.name())) {
                    generalOperations.createGifInChats(this, g);
                }
            }
        }
    }

    private void addMsg(int senderId, String text, MessageTypeEnum type, String date) {
        TextArea textArea = new TextArea();
        String msg = text;
        if (type == MessageTypeEnum.FILE) {
            msg = text.substring(0, text.indexOf(':'));
        }
        textArea.setText(msg);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        if (type == MessageTypeEnum.FILE) {
            textArea.setOnMouseClicked((t) -> {
                String content = text.substring(text.indexOf(':') + 1);
                DirectoryChooser directoryChooser = new DirectoryChooser();
                File file = directoryChooser.showDialog(stage);
                generalOperations.convertStringToFile(textArea.getText(), content, file);
            });
        }

        Korisnik sender = databaseOperations.dohvatiKorisnika(senderId);
        textArea.setTooltip(generalOperations.createToolTip("" + sender.getKorisnickoIme() + " at " + date));

        HBox h = new HBox();
        Label l = new Label(msg);
        l.setFont(Font.font(15));
        h.getChildren().add(l);
        Scene temp = new Scene(h);
        l.applyCss();

        double maxWidth = 210;
        double width = l.prefWidth(-1) + 20;

        if (width > maxWidth) {
            textArea.setMaxWidth(maxWidth);
            textArea.setMinWidth(maxWidth);
        } else {
            textArea.setMaxWidth(width);
            textArea.setMinWidth(width);
        }

        GridPane.setMargin(textArea, new Insets(5, 5, 5, 5));
        if (senderId == korisnik.getId()) {
            sentMessages.add(textArea);
            chatBox.add(textArea, 1, rowIndex, 1, 1);
            GridPane.setHalignment(textArea, HPos.RIGHT);
        } else {
            chatBox.add(textArea, 0, rowIndex, 1, 1);
        }

        String text_color = "";
        if (type == MessageTypeEnum.FILE) {
            text_color = "-fx-text-fill: #2f1499;";
        } else if (type == MessageTypeEnum.ERROR) {
            text_color = "-fx-text-fill: #b30707;";
        }
        textArea.setStyle("-fx-background-color: " + "transparent;"
                + "-fx-font-size : 15px;"
                + text_color);

        textArea.applyCss();

        Node t = textArea.lookup(".text");
        textArea.prefHeightProperty().bind(Bindings.createDoubleBinding(()
                -> t.getBoundsInLocal().getHeight(), t.boundsInLocalProperty()).add(20));

        textArea.lookup(".text").setStyle("-fx-text-alignment: center;");
        textArea.lookup(".viewport").setStyle("-fx-background-color: " + "transparent;");
        if (senderId == korisnik.getId()) {
            updateMessageColor(ChatColorEnum.values()[selectedCet.getBoja()], textArea);
        } else {
            updateMessageColor(ChatColorEnum.GRAY, textArea);
        }
        rowIndex++;
    }

    public void updateChatListView(Cet cet) {
        cetoviKorisnika.add(cet);
        imenaCetovaKorisnika.add(cet.getIme());
        chatList.getItems().add(cet.getIme());
    }

    public void updateTheme(ChatThemeEnum theme) {
        korisnik = databaseOperations.dohvatiKorisnika(korisnik.getId());
        switch (theme) {
            case LIGHT:
                chatsLeftLayout.setStyle(BACKGROUND_COLOR_STYLE + "#ffffff;");
                chatsCenterLayout.setStyle("");
                chatNameLabel.setTextFill(Paint.valueOf(Color.BLACK.toString()));
                chatBox.setStyle("");
                break;
            case DARK:
                chatsLeftLayout.setStyle(BACKGROUND_COLOR_STYLE + "#dcdcdc;");
                chatsCenterLayout.setStyle(BACKGROUND_COLOR_STYLE + "#575757;");
                chatNameLabel.setTextFill(Paint.valueOf(Color.WHITE.toString()));
                chatBox.setStyle(BACKGROUND_COLOR_STYLE + "darkgray");
                break;
        }
    }

    public void updateProfilePicture(Image image) {
        korisnik = databaseOperations.dohvatiKorisnika(korisnik.getId());
        profilePicture.setImage(image);
    }

    public void updateChatName(String chatName) {
        chatNameLabel.setText(chatName);
    }

    public void updateChatColor(ChatColorEnum color) {
        sentMessages.forEach((ta) -> updateMessageColor(color, ta));
    }

    public void selectNewChat() {
        chatList.getSelectionModel().clearSelection();
        selectedIndex = -1;
        selectedCet = null;
        chatList.getSelectionModel().selectLast();
        selectCet();
    }

    private void updateMessageColor(ChatColorEnum color, TextArea ta) {
        ta.lookup(".content").setStyle("-fx-background-color: " + color.getColorCode() + ";\n"
                + "-fx-background-radius: 20px;\n"
                + "-fx-border-radius: 20px;\n"
                + "-fx-border-width: 5px;");
    }

    public void setImageOfGif(Image i) {
        gifView.setImage(i);
    }

    public void logout() {
        if (korisnik == null) {
            return;
        }
        jmsOperations.stopReceiving();
        notificationOperations.removeTrayIcon();
        korisnik = null;
        selectedCet = null;
        selectedIndex = -1;
    }

}
