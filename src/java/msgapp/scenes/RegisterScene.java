package msgapp.scenes;

import msgapp.operations.GeneralOperations;
import msgapp.operations.DatabaseOperations;
import msgapp.entities.Cet;
import msgapp.entities.Korisnik;
import java.io.File;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import msgapp.enums.GifEnum;

public class RegisterScene extends Scene {

    private final DatabaseOperations databaseOperations = new DatabaseOperations();
    private final GeneralOperations generalOperations = new GeneralOperations();

    private final TextField firstNameInput, lastNameInput, usernameInput, emailInput;
    private final PasswordField passInput1, passInput2;
    private final DatePicker dateOfBirthInput;
    private File file = null;
    private final Label filename;

    private final Label firstNameMsg, lastNameMsg, userNameMsg, emailMsg, pass1Msg, pass2Msg, dateOfBirthMsg, msg;

    private final ImageView gifView;
    
    private final Stage stage;

    public RegisterScene(Parent parent, Stage st) {
        super(parent, GeneralOperations.WINDOW_WIDTH, GeneralOperations.WINDOW_HEIGHT);
        stage = st;

        Label registerLabel = generalOperations.createLabel("Register", GeneralOperations.LabelEnum.TITLE, null);
        registerLabel.setFont(Font.font(24));

        firstNameInput = generalOperations.createTextField("first name", GeneralOperations.TextFieldEnum.REGISTER);
        firstNameInput.setTooltip(generalOperations.createToolTip(""
                + "First name should start with first uppercase letter."));
        lastNameInput = generalOperations.createTextField("last name", GeneralOperations.TextFieldEnum.REGISTER);
        lastNameInput.setTooltip(generalOperations.createToolTip(""
                + "Last name should start with first uppercase letter."));
        usernameInput = generalOperations.createTextField("username", GeneralOperations.TextFieldEnum.REGISTER);
        usernameInput.setTooltip(generalOperations.createToolTip(
                "Username constraints:\n"
                + "at least 3 characters\n"
                + "and valid characters are\n"
                + "digits, upper case alphabet, lower case alphabet\n"
                + "and points, dashes and underscores."));
        emailInput = generalOperations.createTextField("email", GeneralOperations.TextFieldEnum.REGISTER);
        emailInput.setTooltip(generalOperations.createToolTip(""
                + "Should be valid email format.\n"
                + "Example: example@example.com"));
        passInput1 = generalOperations.createPasswordField("password", false);
        passInput1.setTooltip(generalOperations.createToolTip(
                "Password constraints:\n"
                + "at least 8 characters and at most 20 characters,\n"
                + "at least one digit, one upper case alphabet,\n"
                + "one lower case alphabet, one special character which includes .!@#$%^&*()_+-=\n"
                + "and doesn’t contain any white space.\n"));
        passInput2 = generalOperations.createPasswordField("confirm password", false);
        dateOfBirthInput = generalOperations.createDatePicker("birthday");

        FileChooser fileChooser = generalOperations.createProfilePictureChooser();
        filename = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_FILENAME, Color.NAVY);
        Button openButton = generalOperations.createButton("profile picture", GeneralOperations.ButtonTypeEnum.REGISTER_PROFILE_PIC, false, (t) -> {
            file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                filename.setText(file.getName());
            } else {
                filename.setText("");
            }
        });
        openButton.setTooltip(generalOperations.createToolTip("Maximum size 16777215 bytes (16 MB)"));

        firstNameMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        lastNameMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        userNameMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        emailMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        pass1Msg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        pass2Msg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        dateOfBirthMsg = generalOperations.createLabel("", GeneralOperations.LabelEnum.REGISTER_ERROR, Color.RED);
        msg = generalOperations.createLabel("", GeneralOperations.LabelEnum.ERROR_MSG, Color.RED);

        Button backRegisterButton = generalOperations.createButton("BACK", GeneralOperations.ButtonTypeEnum.REGISTER_BACK, false, (t)
                -> stage.setScene(new LoginScene(new FlowPane(), stage)));

        Button registerButton = generalOperations.createButton("REGISTER", GeneralOperations.ButtonTypeEnum.REGISTER, true, (t) -> doRegister());

        FlowPane registerLayout = generalOperations.createFlowPane(GeneralOperations.FlowPaneEnum.REGISTER, true);
        registerLayout.getChildren().addAll(registerLabel, firstNameInput, firstNameMsg, lastNameInput, lastNameMsg,
                usernameInput, userNameMsg, emailInput, emailMsg, passInput1, pass1Msg, passInput2, pass2Msg,
                dateOfBirthInput, dateOfBirthMsg, openButton, filename, backRegisterButton, registerButton, msg);

        gifView = new ImageView();
        StackPane.setMargin(gifView, new Insets((GeneralOperations.WINDOW_HEIGHT - GifEnum.CONFETTI.getHeight()) / 4,
                (GeneralOperations.WINDOW_WIDTH - GifEnum.CONFETTI.getWidth()) / 2,
                (GeneralOperations.WINDOW_HEIGHT - GifEnum.CONFETTI.getHeight()) * 3 / 4,
                (GeneralOperations.WINDOW_WIDTH - GifEnum.CONFETTI.getWidth()) / 2));
        StackPane stackPane = new StackPane(registerLayout, gifView);
        this.setRoot(stackPane);
    }

    private void clearMsgs() {
        firstNameMsg.setText("");
        lastNameMsg.setText("");
        userNameMsg.setText("");
        emailMsg.setText("");
        pass1Msg.setText("");
        pass2Msg.setText("");
        dateOfBirthMsg.setText("");
        msg.setText("");
    }
    
    private boolean checkIfValid(Label msg,String text, boolean isEquals,String regex){
        if (text.isEmpty()) {
            msg.setText("required");
            return false;
        } else if (!isEquals && !text.matches(regex)) {
            msg.setText("not valid");
            return false;
        } else if (isEquals && !text.equals(regex)){
            msg.setText("not matching");
            return false;
        }
        return true;
    }

    private void doRegister() {
        String firstName = firstNameInput.getText();
        String lastName = lastNameInput.getText();
        String username = usernameInput.getText();
        String email = emailInput.getText();
        String pass = passInput1.getText();
        String pass2 = passInput2.getText();
        LocalDate localDate = dateOfBirthInput.getValue();
        Date dateOfBirth = null;
        if (localDate != null) {
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            dateOfBirth = Date.from(instant);
        }

        clearMsgs();
        boolean ok = true;

        if(!checkIfValid(firstNameMsg, firstName, false, "^[A-Z][a-zA-Z]+$"))
            ok = false;
        if (!checkIfValid(lastNameMsg, lastName, false, "^[A-Z][a-zA-Z]+$"))
            ok = false;
        if (!checkIfValid(userNameMsg, username, false, "^[a-zA-Z0-9\\._\\-]{3,}$"))
            ok = false;
        if (!checkIfValid(emailMsg, email, false, "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
            ok = false;
        if (!checkIfValid(pass1Msg, pass, false, "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\\.!@#$%^&*\\(\\)_+\\-=])(?=\\S+$).{8,20}$"))
            ok = false;
        if (!checkIfValid(pass2Msg, pass2, true, pass))
            ok = false;
        if (dateOfBirth == null) {
            ok = false;
            dateOfBirthMsg.setText("required");
        }

        byte[] image = generalOperations.convertImageFileToArrayOfBytes(file);

        String poruka = databaseOperations.proveriDaLiKorisnikPostoji(username, email);
        if (!poruka.isEmpty()) {
            ok = false;
            msg.setText(msg.getText() + poruka);
        }

        if (!ok) {
            return;
        }

        Korisnik korisnik = databaseOperations.napraviKorisnika(firstName, lastName, username, email, pass, dateOfBirth, image);
        if (korisnik == null) {
            msg.setText("Error occurred. Please try again later.");
            return;
        }
        Cet cet = databaseOperations.napraviCet("just you");
        databaseOperations.napraviPripadnostCetu(korisnik, cet);

        generalOperations.createGifInRegister(this);
        msg.setText("Successful!");
    }

    public void setImageOfGif(Image i) {
        gifView.setImage(i);
    }

}
