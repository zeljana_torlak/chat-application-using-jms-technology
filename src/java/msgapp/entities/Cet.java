/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msgapp.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "cet")
@NamedQueries({
    @NamedQuery(name = "Cet.findAll", query = "SELECT c FROM Cet c"),
    @NamedQuery(name = "Cet.findById", query = "SELECT c FROM Cet c WHERE c.id = :id"),
    @NamedQuery(name = "Cet.findByIme", query = "SELECT c FROM Cet c WHERE c.ime = :ime"),
    @NamedQuery(name = "Cet.findByBoja", query = "SELECT c FROM Cet c WHERE c.boja = :boja")})
public class Cet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ime")
    private String ime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "boja")
    private int boja;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCet")
    private List<PripadnostCetu> pripadnostCetuList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCet")
    private List<Poruka> porukaList;

    public Cet() {
    }

    public Cet(Integer id) {
        this.id = id;
    }

    public Cet(Integer id, String ime, int boja) {
        this.id = id;
        this.ime = ime;
        this.boja = boja;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public int getBoja() {
        return boja;
    }

    public void setBoja(int boja) {
        this.boja = boja;
    }

    public List<PripadnostCetu> getPripadnostCetuList() {
        return pripadnostCetuList;
    }

    public void setPripadnostCetuList(List<PripadnostCetu> pripadnostCetuList) {
        this.pripadnostCetuList = pripadnostCetuList;
    }

    public List<Poruka> getPorukaList() {
        return porukaList;
    }

    public void setPorukaList(List<Poruka> porukaList) {
        this.porukaList = porukaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cet)) {
            return false;
        }
        Cet other = (Cet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Cet[ id=" + id + " ]";
    }
    
}
