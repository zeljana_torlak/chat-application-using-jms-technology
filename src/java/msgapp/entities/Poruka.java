/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msgapp.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author hp-650g1
 */
@Entity
@Table(name = "poruka")
@NamedQueries({
    @NamedQuery(name = "Poruka.findAll", query = "SELECT p FROM Poruka p"),
    @NamedQuery(name = "Poruka.findById", query = "SELECT p FROM Poruka p WHERE p.id = :id"),
    @NamedQuery(name = "Poruka.findByTip", query = "SELECT p FROM Poruka p WHERE p.tip = :tip"),
    @NamedQuery(name = "Poruka.findByVreme", query = "SELECT p FROM Poruka p WHERE p.vreme = :vreme")})
public class Poruka implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 16777215)
    @Column(name = "sadrzaj")
    private String sadrzaj;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tip")
    private int tip;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vreme")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vreme;
    @JoinColumn(name = "idCet", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cet idCet;
    @JoinColumn(name = "idPosiljaoc", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Korisnik idPosiljaoc;

    public Poruka() {
    }

    public Poruka(Integer id) {
        this.id = id;
    }

    public Poruka(Integer id, String sadrzaj, int tip, Date vreme) {
        this.id = id;
        this.sadrzaj = sadrzaj;
        this.tip = tip;
        this.vreme = vreme;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }

    public int getTip() {
        return tip;
    }

    public void setTip(int tip) {
        this.tip = tip;
    }

    public Date getVreme() {
        return vreme;
    }

    public void setVreme(Date vreme) {
        this.vreme = vreme;
    }

    public Cet getIdCet() {
        return idCet;
    }

    public void setIdCet(Cet idCet) {
        this.idCet = idCet;
    }

    public Korisnik getIdPosiljaoc() {
        return idPosiljaoc;
    }

    public void setIdPosiljaoc(Korisnik idPosiljaoc) {
        this.idPosiljaoc = idPosiljaoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Poruka)) {
            return false;
        }
        Poruka other = (Poruka) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Poruka[ id=" + id + " ]";
    }
    
}
